import 'dart:io';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:uuid/uuid.dart';
import 'package:video_compress/video_compress.dart';


import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:media_firebase/models/video.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_player/video_player.dart';

class VideoController extends GetxController {
  var videos = List<MediaVideo>().obs;
  var uuid = Uuid();
  static Subscription _subscription;
  static ProgressDialog pr;

  void onInit() async {
    await Firebase.initializeApp();
    ListResult result =
        await FirebaseStorage.instance.ref().child('Videos').list();
    result.items.forEach((Reference ref) async {
      var vid = MediaVideo(
          name: ref.name,
          size: (await ref.getMetadata()).size.toString(),
          urlDownload: await ref.getDownloadURL());

      vid.controller = VideoPlayerController.network(vid.urlDownload);
      vid.initializeVideoPlayerFuture = vid.controller.initialize();

      vid.controller.setLooping(true);

      videos.add(vid);
    });
    super.onInit();
  }

  uploadVideo(
      {bool compress: false,
      VideoQuality quality = VideoQuality.DefaultQuality}) async {
    try {
      final _storage = FirebaseStorage.instance;
      final _picker = ImagePicker();
      PickedFile video;

      //Check Permissions
      await Permission.photos.request();

      var permissionStatus = await Permission.photos.status;

      if (permissionStatus.isGranted) {
        //Select Image
        video = await _picker.getVideo(source: ImageSource.gallery);
        var file = File(video.path);
        // final _flutterVideoCompress = FlutterVideoCompress();
        if (compress) {
          VideoController._subscription =
              VideoCompress.compressProgress$.subscribe((progress) {
            print('pppppp ${progress.toStringAsFixed(1)}');
            VideoController.pr.update(
                progress: double.parse(progress.toStringAsFixed(1)),
                message: 'Compressing..');
          });
          VideoController.pr.show();

          MediaInfo mediaInfo = await VideoCompress.compressVideo(
            file.path,
            quality: quality,
            deleteOrigin: false, // It's false by default
          );

          pr.hide();
          _subscription.unsubscribe();
          // final info = await _flutterVideoCompress.compressVideo(
          //   file.path,
          //   quality:VideoQuality.DefaultQuality,
          //   deleteOrigin: false,includeAudio: true
          // );
          file = mediaInfo.file;
          Get.snackbar('Image Upload Worker', 'Image Compressed Successfuly!',
              isDismissible: true,
              icon: Icon(Icons.image_rounded),
              snackPosition: SnackPosition.BOTTOM,
              dismissDirection: SnackDismissDirection.HORIZONTAL);
        }

        if (video != null) {
          //Upload to Firebase
          var snapshot = await _storage
              .ref()
              .child('Videos')
              .child('${uuid.v4()}.mp4')
              .putFile(file);

          var downloadUrl = await snapshot.ref.getDownloadURL();

          videos.add(MediaVideo(
              name: snapshot.ref.name,
              size: snapshot.totalBytes.toString(),
              urlDownload: downloadUrl));
          Get.snackbar('Video Upload Worker', 'Video Uploaded Successfuly!',
              isDismissible: true,
              icon: Icon(Icons.video_settings),
              snackPosition: SnackPosition.BOTTOM,
              dismissDirection: SnackDismissDirection.HORIZONTAL);
        } else {
          print('No Path Received');
        }
      } else {
        print('Grant Permissions and try again');
      }
    } catch (e) {
      print(e.toString());
    }
  }

  void deleteVideo(MediaVideo vid) async {
    await FirebaseStorage.instance
        .ref()
        .child('Videos')
        .child(vid.name)
        .delete();
    videos.remove(vid);
  }
}
