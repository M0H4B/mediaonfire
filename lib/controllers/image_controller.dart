import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:uuid/uuid.dart';

// Create uuid object

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:media_firebase/models/image.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class ImageController extends GetxController {
  var images = List<MediaImage>().obs;
  var uuid = Uuid();

  void onInit() async {
    await Firebase.initializeApp();
    ListResult result =
        await FirebaseStorage.instance.ref().child('Images').list();
    result.items.forEach((Reference ref) async {
      images.add(MediaImage(
          name: ref.name,
          size: (await ref.getMetadata()).size.toString(),
          urlDownload: await ref.getDownloadURL()));
    });
    super.onInit();
  }

  uploadImage({bool compress: false, int quality = 90}) async {
    try {
      final _storage = FirebaseStorage.instance;
      final _picker = ImagePicker();
      PickedFile image;

      //Check Permissions
      await Permission.photos.request();

      var permissionStatus = await Permission.photos.status;

      if (permissionStatus.isGranted) {
        //Select Image
        image = await _picker.getImage(source: ImageSource.gallery);
        var file = File(image.path);
        if (compress) {
          var decodedImage = await decodeImageFromList(file.readAsBytesSync());
          file = await FlutterNativeImage.compressImage(file.path,
              quality: quality,
              targetHeight: decodedImage.height,
              targetWidth: decodedImage.width);
          Get.snackbar('Image Upload Worker', 'Image Compressed Successfuly!',
              isDismissible: true,
              icon: Icon(Icons.image_rounded),
              snackPosition: SnackPosition.BOTTOM,
              dismissDirection: SnackDismissDirection.HORIZONTAL);
        }
        if (image != null) {
          //Upload to Firebase
          var snapshot = await _storage
              .ref()
              .child('Images')
              .child('${uuid.v4()}.png')
              .putFile(file);

          var downloadUrl = await snapshot.ref.getDownloadURL();

          images.add(MediaImage(
              name: snapshot.ref.name,
              size: snapshot.totalBytes.toString(),
              urlDownload: downloadUrl));
          Get.snackbar('Image Upload Worker', 'Image Uploaded Successfuly!',
              isDismissible: true,
              icon: Icon(Icons.image_rounded),
              snackPosition: SnackPosition.BOTTOM,
              dismissDirection: SnackDismissDirection.HORIZONTAL);
        } else {
          print('No Path Received');
        }
      } else {
        print('Grant Permissions and try again');
      }
    } catch (e) {
      print(e.toString());
    }
  }

  void deleteImage(MediaImage imag) async {
    await FirebaseStorage.instance
        .ref()
        .child('Images')
        .child(imag.name)
        .delete();
    images.remove(imag);
  }
}
