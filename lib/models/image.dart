class MediaImage {
  final String name;

  final String urlDownload;

  final String size;

  MediaImage({this.name,this.urlDownload,this.size});
}
