import 'package:get/get.dart';
import 'package:video_player/video_player.dart';

class MediaVideo  extends GetxController{
  final String name;

  final String urlDownload;

  final String size;
  VideoPlayerController controller;
  Future<void> initializeVideoPlayerFuture;
  RxBool isPlaying = false.obs;
  MediaVideo({this.name,this.urlDownload,this.size});
}
