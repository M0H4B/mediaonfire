import 'package:flutter/material.dart';
import 'package:media_firebase/test.dart';
import 'package:media_firebase/test1.dart';
import 'package:media_firebase/views/videos_view.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        elevation: 10,
        child: Container(
          height: double.maxFinite,
          child: Stack(children: <Widget>[
            Positioned(
              child: Column(
                children: <Widget>[
                  AppBar(
                    title: Text('Test Application',
                      style: TextStyle(
                        fontSize: 17,
                      ),
                    ),
                    leading: Container(
                      width: 140.0,
                      height: 140.0,
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  Divider(),
                  ListTile(
                    leading: Icon(
                      Icons.image,
                    ),
                    title: Text(
                      'Images Form',
                    ),
                    onTap: () {
                      Navigator.of(context).pushReplacementNamed('/');
                    },
                  ),
                  const Divider(),
                  const Divider(),
                  ListTile(
                    leading: Icon(
                      Icons.videocam_sharp,
                    ),
                    title: Text(
                      'Videos Form',
                    ),
                    onTap: () {

                      Navigator.of(context).pushNamed(VideoView.routeName);
                    },
                  ),
                  const Divider(),

                  const Divider(),
                  // ListTile(
                  //   leading: Icon(
                  //     Icons.text_snippet,
                  //   ),
                  //   title: Text(
                  //     'Test Form',
                  //   ),
                  //   onTap: () {

                  //     // Navigator.of(context).pushNamed(MyAppp.routeName);
                  //   },
                  // ),
                  const Divider(),
                ],
              ),
            ),
            new Positioned(
              child: new Align(
                alignment: FractionalOffset.bottomLeft,
                child: Container(
                    margin: EdgeInsets.all(10),
                    child: Text(
                      '',
                    )),
              ),
            )
          ]),
        ));
  }
}
