import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:media_firebase/controllers/image_controller.dart';
import '../../drawer.dart';

import 'package:numberpicker/numberpicker.dart';

class ImageView extends StatelessWidget {
  static var routeName = '/images';

  final _imageController = Get.put(ImageController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('View Images'),
        actions: [],
      ),
      drawer: AppDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: GetX<ImageController>(builder: (snapshot) {
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 3),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    // Lets create a coffee model and populate it
                    itemCount: snapshot.images.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 4.0,
                        mainAxisSpacing: 4.0),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {},
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white,
                                  offset: Offset(0, 2),
                                  blurRadius: 4.0,
                                )
                              ],
                            ),
                            child: Column(
                              children: [
                                Expanded(
                                  child: (snapshot.images[index].urlDownload !=
                                          null)
                                      ? Image.network(
                                          snapshot.images[index].urlDownload,
                                          fit: BoxFit.contain,
                                        )
                                      : Placeholder(
                                          fallbackHeight: double.infinity,
                                          fallbackWidth: double.infinity),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 1.0,
                                    vertical: 1.0,
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        snapshot.images[index].name,
                                        style: TextStyle(
                                          fontSize: 13.0,
                                          color: Colors.red,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                          vertical: 8.0,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              snapshot.images[index].size +
                                                  "\\Bytes",
                                              style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                _imageController.deleteImage(
                                                    snapshot.images[index]);
                                              },
                                              child: Icon(
                                                Icons.delete_forever,
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              }),
            ),
          ),
          RaisedButton(
            child: Text('Upload Image'),
            color: Colors.lightBlue,
            onPressed: () {
              _imageController.uploadImage();
            },
          ),
          RaisedButton(
            onPressed: () async {
              var val = 50;
              await showDialog(
                  child: AlertDialog(
                    title: Text('Choice Compression level from 1 - 100'),
                    content: Container(
                        child: NumberPicker.integer(
                      initialValue: val,
                      step: 10,
                      selectedTextStyle: TextStyle(color: Colors.blue),
                      highlightSelectedValue: true,
                      listViewWidth: 200,
                      minValue: 0,
                      maxValue: 100,
                      onChanged: (_val) {
                        val = _val;
                      },
                    )),
                    actions: [
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                        child: Text('dismiss'),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                          _imageController.uploadImage(
                              compress: true, quality: val);
                        },
                        child: Text('upload'),
                      )
                    ],
                  ),
                  context: context);
            },
            child: Text("Upload Image With Compression "),
          ),
        ],
      ),
    );
  }
}
