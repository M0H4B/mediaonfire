import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:media_firebase/controllers/video_controller.dart';
import 'package:video_compress/video_compress.dart';
import 'package:video_player/video_player.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../drawer.dart';

class VideoView extends StatelessWidget {
  static var routeName = '/videos';

  @override
  Widget build(BuildContext context) {
  VideoController.pr = ProgressDialog(context,type: ProgressDialogType.Download,isDismissible: false);

    final _videoController = Get.put(VideoController());
    return Scaffold(
      appBar: AppBar(
        title: Text('View Videos'),
    
      ),
      drawer: AppDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: GetX<VideoController>(builder: (snapshot) {
                return Container(
                  margin: EdgeInsets.all(3),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height*0.6,
                  child: GridView.builder(
                    itemCount: snapshot.videos.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                       ),
                    itemBuilder: (BuildContext context, int index) {
                      return ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.white,
                                offset: Offset(0, 2),
                                blurRadius: 4.0,
                              )
                            ],
                          ),
                          child: Column(
                            children: [
                              Expanded(
                                child: Visibility(
                                  visible:
                                      snapshot.videos[index].controller != null,
                                  child: FutureBuilder(
                                    future: snapshot.videos[index]
                                        .initializeVideoPlayerFuture,
                                    builder: (context, _snapshot) {
                                      if (_snapshot.connectionState ==
                                          ConnectionState.done) {
                                        return AspectRatio(
                                          aspectRatio: snapshot.videos[index]
                                              .controller.value.aspectRatio,
                                          child: VideoPlayer(snapshot
                                              .videos[index].controller),
                                        );
                                      } else {
                                        return Center(
                                            child: CircularProgressIndicator());
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 1.0,
                                  vertical: 1.0,
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      snapshot.videos[index].name,
                                      style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.red,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                        vertical: 8.0,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 7,
                                            child: Text(
                                              snapshot.videos[index].size +
                                                  "\\Bytes",
                                              style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                              flex: 1,
                                              child: Obx(
                                                () => GestureDetector(
                                                  onTap: () {
                                                    if (snapshot
                                                        .videos[index]
                                                        .controller
                                                        .value
                                                        .isPlaying) {
                                                      snapshot.videos[index]
                                                          .controller
                                                          .pause();
                                                    } else {
                                                      snapshot.videos[index]
                                                          .controller
                                                          .play();
                                                    }
                                                    snapshot
                                                        .videos[index].isPlaying
                                                        .toggle();
                                                  },
                                                  child: Icon(
                                                    snapshot.videos[index]
                                                            .isPlaying.isTrue
                                                        ? Icons.pause
                                                        : Icons.play_arrow,
                                                  ),
                                                ),
                                              )),
                                          Expanded(
                                            flex: 1,
                                            child: GestureDetector(
                                              onTap: () {
                                                _videoController.deleteVideo(
                                                    snapshot.videos[index]);
                                              },
                                              child: Icon(
                                                Icons.delete_forever,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                );
              }),
            ),
          ),
          RaisedButton(
            child: Text('Upload Video'),
            color: Colors.lightBlue,
            onPressed: () {
              _videoController.uploadVideo();
            },
          ),
          RaisedButton(
            onPressed: () async {
              await showDialog(
                  child: AlertDialog(
                    title: Text('Choice Video Quality'),
                    content: QualityRadioList(),
                    actions: [
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                        child: Text('dismiss'),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                          _videoController.uploadVideo(
                              compress: true,
                              quality: QualityRadioList.quality);



                        },
                        child: Text('upload'),
                      )
                    ],
                  ),
                  context: context);
            },
            child: Text("Upload Video With Compression "),
          ),
        ],
      ),
    );
  }
}

enum VideoQualityRadio {
  defaultQuality,
  lowQuality,
  mediumQuality,
  highestQuality
}

class QualityRadioList extends StatefulWidget {
  static VideoQuality quality;
  QualityRadioList();
  @override
  _QualityRadioListState createState() => _QualityRadioListState();
}

class _QualityRadioListState extends State<QualityRadioList> {
  VideoQualityRadio _character = VideoQualityRadio.defaultQuality;

  void changeSelection(VideoQualityRadio val, VideoQuality qt) {
    setState(() {
      _character = val;
    });
    QualityRadioList.quality = qt;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 260,
      child: Column(
        children: [
          RadioListTile<VideoQualityRadio>(
            title: const Text('DefaultQuality'),
            value: VideoQualityRadio.defaultQuality,
            groupValue: _character,
            onChanged: (val) =>
                changeSelection(val, VideoQuality.DefaultQuality),
          ),
          RadioListTile<VideoQualityRadio>(
            title: const Text('LowQuality'),
            value: VideoQualityRadio.lowQuality,
            groupValue: _character,
            onChanged: (val) => changeSelection(val, VideoQuality.LowQuality),
          ),
          RadioListTile<VideoQualityRadio>(
            title: const Text('MediumQuality'),
            value: VideoQualityRadio.mediumQuality,
            groupValue: _character,
            onChanged: (val) =>
                changeSelection(val, VideoQuality.MediumQuality),
          ),
          RadioListTile<VideoQualityRadio>(
            title: const Text('HighestQuality'),
            value: VideoQualityRadio.highestQuality,
            groupValue: _character,
            onChanged: (val) =>
                changeSelection(val, VideoQuality.HighestQuality),
          ),
        ],
      ),
    );
  }
}
