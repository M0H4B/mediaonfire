import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:media_firebase/test.dart';
import 'package:media_firebase/test1.dart';
import 'package:media_firebase/views/image_view/images_view.dart';
import 'package:media_firebase/views/videos_view.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: ImageView(),
      routes: {
        TaskManager.routeName: (ctx) => TaskManager(),
        VideoView.routeName:(ctx) => VideoView(),
        ImageView.routeName:(ctx) => ImageView(),
        // MyAppp.routeName:(ctx) => MyAppp(),
      },
    );
  }
}
